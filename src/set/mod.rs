mod hash_set;

/// A set.
pub trait Set<'s, V: 's> {
    type Iter: Iterator<Item = &'s V>;

    /// Returns an iterator that iterates over all items in this set.
    fn iter(&'s self) -> Self::Iter;

    /// Inserts a new value into this set, optionally returning an old value it
    /// replaces.
    fn insert(&'s mut self, value: V) -> Option<V>;

    /// Removes a value from this set, returning it.
    fn remove(&'s mut self, value: &V) -> Option<V>;

    /// Returns whether this set contains a value.
    fn contains(&'s self, value: &V) -> bool;
}
