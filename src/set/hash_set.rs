use Set;
use std::collections::hash_set::{HashSet, Iter};
use std::hash::Hash;

impl<'s, V> Set<'s, V> for HashSet<V>
where
    V: 's + Eq + Hash,
{
    type Iter = Iter<'s, V>;

    fn iter(&'s self) -> Self::Iter {
        HashSet::iter(self)
    }

    fn insert(&'s mut self, value: V) -> Option<V> {
        HashSet::replace(self, value)
    }

    fn remove(&'s mut self, value: &V) -> Option<V> {
        HashSet::take(self, value)
    }

    fn contains(&'s self, value: &V) -> bool {
        HashSet::contains(self, value)
    }
}
