mod hash_map;

/// A map associating keys with values.
pub trait Map<'m, K: 'm, V: 'm> {
    type Keys: Iterator<Item = &'m K>;
    type Values: Iterator<Item = &'m V>;
    type ValuesMut: Iterator<Item = &'m mut V>;
    type Iter: Iterator<Item = (&'m K, &'m V)>;
    type IterMut: Iterator<Item = (&'m K, &'m mut V)>;

    /// Returns an iterator over all keys in this `Map`.
    fn keys(&'m self) -> Self::Keys;

    /// Returns an iterator over all values in this `Map`.
    fn values(&'m self) -> Self::Values;

    /// Returns an iterator over mutable borrows to all values in this `Map`.
    fn values_mut(&'m mut self) -> Self::ValuesMut;

    /// Returns an iterator over all keys and their values in this `Map`.
    fn iter(&'m self) -> Self::Iter;

    /// Returns an iterator over all keys and their values as mutable borrows.
    fn iter_mut(&'m mut self) -> Self::IterMut;

    /// Borrows value from its key.
    fn get(&'m self, key: &K) -> Option<&'m V>;

    /// Borrows a value mutably from its key.
    fn get_mut(&'m mut self, key: &K) -> Option<&'m mut V>;

    /// Inserts a value into this `Map`. If the key was already used, it
    /// returns the old key and value pair.
    fn insert(&'m mut self, key: K, value: V) -> Option<(K, V)>;

    /// Removes a value from this `Map`, returning it.
    fn remove(&'m mut self, key: &K) -> Option<(K, V)>;

    /// Checks whether this `Map` contains a value at the key.
    fn contains_key(&'m self, key: &K) -> bool {
        self.get(key).is_some()
    }
}


// impl<'s, V, Keys, T> Set<'s, V> for T
// where
//     V: 's,
//     T: Map<'s, V, (), Keys = Keys>,
//     Keys: Iterator<Item = &'s V>,
// {
//     type Iter = Keys;

//     fn iter(&'s self) -> Self::Iter {
//         Map::keys(self)
//     }

//     fn insert(&'s mut self, value: V) -> Option<V> {
//         Map::insert(self, value, ()).map(|(value, _)| value)
//     }

//     fn remove(&'s mut self, value: &'s V) -> Option<V> {
//         Map::remove(self, value).map(|(value, _)| value)
//     }

//     fn contains(&'s self, value: &'s V) -> bool {
//         Map::contains_key(self, value)
//     }
// }
