use Map;
use std::collections::hash_map::{Entry, HashMap, Iter, IterMut, Keys, Values, ValuesMut};
use std::hash::Hash;

// We need the key to implement Clone because HashMap does not allow us to
// remove an entry without losing the key.
impl<'m, K: 'm, V: 'm> Map<'m, K, V> for HashMap<K, V>
where
    K: Hash + Eq + Clone,
{
    type Keys = Keys<'m, K, V>;
    type Values = Values<'m, K, V>;
    type ValuesMut = ValuesMut<'m, K, V>;
    type Iter = Iter<'m, K, V>;
    type IterMut = IterMut<'m, K, V>;

    fn keys(&'m self) -> Self::Keys {
        HashMap::keys(self)
    }

    fn values(&'m self) -> Self::Values {
        HashMap::values(self)
    }

    fn values_mut(&'m mut self) -> Self::ValuesMut {
        HashMap::values_mut(self)
    }

    fn iter(&'m self) -> Self::Iter {
        HashMap::iter(self)
    }

    fn iter_mut(&'m mut self) -> Self::IterMut {
        HashMap::iter_mut(self)
    }

    fn get(&'m self, key: &K) -> Option<&'m V> {
        HashMap::get(self, key)
    }

    fn get_mut(&'m mut self, key: &K) -> Option<&'m mut V> {
        HashMap::get_mut(self, key)
    }

    fn insert(&'m mut self, key: K, value: V) -> Option<(K, V)> {
        let to_remove = Map::remove(self, &key);
        HashMap::insert(self, key, value);
        to_remove
    }

    fn remove(&'m mut self, key: &K) -> Option<(K, V)> {
        if let Entry::Occupied(entry) = HashMap::entry(self, key.clone()) {
            Some(entry.remove_entry())
        } else {
            None
        }
    }
}
